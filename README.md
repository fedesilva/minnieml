
### MinnieML: A statically typed language with an ML resemblance.

It is an experiment and a learning tool for the author; it will try to be as low level as possible 
while providing a modern typesystem and secure access to raw memory.

Some of the planned features, none of which exists anywhere but on the author's mind:

+ Compiles to native code via llvm.

+ Libs can be distributed in native form or in pods (more flexible, must compile).

+ Functional with support for imperative blocks ( see monadic notation ).

+ HDM based type inference.

+ Unboxed primitives. 

+ Product and sum types.

+ Parametric polymorphism.

+ Protocols (a form of typeclass-like construct).

+ Module system. Higher Order Modules; modules are values and can be parameterized by other values and types.

+ Pattern matching. With protocol based extension mechanism.

+ Composites: Inheritance that is not subtyping. Subtyping can be encoded with protocols.

+ Structural types without subtyping. (This might go away ...)

+ No automatic or implicit casting whatsoever.  A fundamental protocol controls type conversions and must be invoked explicitely.

+ Effects system: functions and procs are not the same.

+ Monadic sugar. Begin/End and Do/End blocks provide imperative like sequencing capabilities.

+ Ownership protocol. Allows control of linear and or unique usage.

+ Memory allocation inference. Region based memory allocation; user can interact declaratively.

+ MutableCell. Type that allows mutation of it contained value. Unique ownership. 

+ MemoryChunk and related types and protocols for unmanaged memory management and easy and secure ffi.

+ Type driven CodeGen. Flexible type level notation to generate types and skeleton implementations.
  Think `deriving` keyword that can infer some implementations.

+ Type rules. A constraints system that allows to inspect and influence the static environment.

+ Type tags: User can `tag` a value. Tags are first class types.


